package jtracerlogrus

import (
	"github.com/opentracing/opentracing-go/log"
	"github.com/sirupsen/logrus"
	"github.com/uber/jaeger-client-go"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

type Hook struct {
	levels []logrus.Level
}

func NewHook(levels []logrus.Level) *Hook {
	return &Hook{levels: levels}
}

func (h *Hook) Levels() []logrus.Level {
	return h.levels
}

func (h *Hook) Fire(entry *logrus.Entry) error {
	if entry.Context == nil {
		return nil
	}

	span := jtracer.FromContext(entry.Context, jtracer.WithType("Logrus Hook"))
	defer span.Finish()

	df := newDataField(entry.Data)

	if err, ok := df.getError(); ok {
		span.LogFields(log.Error(err))
	}

	for k, v := range df.data {
		if df.isOmit(k) {
			continue // skip already used
		}

		span.LogFields(log.Object(k, v))
	}

	if sc, ok := span.Context().(jaeger.SpanContext); ok {
		entry.Data["Span ID"] = sc.SpanID().String()
		entry.Data["Trace ID"] = sc.TraceID().String()
	}

	return nil
}
