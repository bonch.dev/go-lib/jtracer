package jtracerlogrus

import (
	"github.com/sirupsen/logrus"
	"github.com/uber/jaeger-client-go/config"
)

type Logger struct {
	logger logrus.FieldLogger
}

func NewLogger(logger logrus.FieldLogger) *Logger {
	return &Logger{logger: logger}
}

func (l *Logger) ToOption() config.Option {
	return config.Logger(l)
}

func (l *Logger) Error(msg string) {
	l.logger.Error(msg)
}

func (l *Logger) Infof(msg string, args ...interface{}) {
	l.logger.Infof(msg, args...)
}
