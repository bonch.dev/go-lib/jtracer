package jtracer

import (
	"time"
)

type EmptyCtx int

func (*EmptyCtx) Deadline() (deadline time.Time, ok bool) {
	return
}

func (*EmptyCtx) Done() <-chan struct{} {
	return nil
}

func (*EmptyCtx) Err() error {
	return nil
}

func (*EmptyCtx) Value(key interface{}) interface{} {
	return nil
}

func (*EmptyCtx) String() string {
	return "empty tracer Context"
}
