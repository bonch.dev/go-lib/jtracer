package jtracer_test

import (
	"context"
	"testing"

	"github.com/opentracing/opentracing-go"
	"github.com/stretchr/testify/assert"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

func TestFromContext(t *testing.T) {
	t.Run("FromContext - Returns span from empty context", func(t *testing.T) {
		ctx := context.Background()

		span := jtracer.FromContext(ctx, jtracer.WithType("Test"))

		assert.NotNil(t, span)
	})

	t.Run("FromContext - Returns span from injected context", func(t *testing.T) {
		ctx := context.Background()
		parentSpan := jtracer.FromContext(ctx, jtracer.WithType("Test - ParentSpan"))
		assert.NotNil(t, parentSpan)

		ctx = opentracing.ContextWithSpan(ctx, parentSpan)

		span := jtracer.FromContext(ctx, jtracer.WithType("Test"))

		assert.NotNil(t, span)
	})
}

func TestWithContext(t *testing.T) {
	t.Run("WithContext", func(t *testing.T) {
		ctx := context.Background()
		parentSpan := jtracer.FromContext(ctx, jtracer.WithType("Test - ParentSpan"))
		assert.NotNil(t, parentSpan)
		ctx = opentracing.ContextWithSpan(ctx, parentSpan)

		ctx, span := jtracer.WithContext(ctx, jtracer.WithType("Test"))

		assert.NotNil(t, ctx)
		assert.NotNil(t, span)
		assert.Equal(t, span, opentracing.SpanFromContext(ctx))
	})
}
