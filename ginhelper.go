package jtracer

import (
	"context"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

var GinJaegerSpanKey = "JaegerSpan"

func fromGinContext(ctx context.Context) opentracing.Span {
	if span, ok := ctx.Value(GinJaegerSpanKey).(opentracing.Span); ok {
		return span
	}

	return nil
}

// Deprecated: use FromContext instead
func FromGinContext(ctx *gin.Context, t Type) opentracing.Span {
	return FromContext(ctx, t)
}

// ToGinContext injects span to *gin.Context.
func ToGinContext(ctx *gin.Context, span opentracing.Span) *gin.Context {
	ctx.Set(GinJaegerSpanKey, span)

	return ctx
}

// WithGinContext returns current *gin.Context with injected in it span information and span itself.
func WithGinContext(ctx *gin.Context, t Type) (*gin.Context, opentracing.Span) {
	span := FromContext(ctx, t)

	return ToGinContext(ctx, span), span
}
