package jtracer

import (
	"context"

	"github.com/opentracing/opentracing-go"
)

// Deprecated: not used anymore
func FromContextOrNil(ctx context.Context) opentracing.Span {
	return extractSpanFromContext(ctx)
}

// FromContextWithTracer returns an opentracing.Span with information about parents spans, if then set in given context.Context
// It generates from given tracer
func FromContextWithTracer(ctx context.Context, t Type, tracer opentracing.Tracer) opentracing.Span {
	var (
		startSpanOptions = make([]opentracing.StartSpanOption, 0)
	)

	if parentSpan := extractSpanFromContext(ctx); parentSpan != nil {
		startSpanOptions = append(startSpanOptions, opentracing.ChildOf(parentSpan.Context()))
	}

	return tracer.
		StartSpan(t.String(), startSpanOptions...).
		SetTag("Type", t.Type())
}

// FromContext returns an opentracing.Span with information about parents spans, if then set in given context.Context
func FromContext(ctx context.Context, t Type) opentracing.Span {
	var (
		startSpanOptions = make([]opentracing.StartSpanOption, 0)
	)

	if parentSpan := extractSpanFromContext(ctx); parentSpan != nil {
		startSpanOptions = append(startSpanOptions, opentracing.ChildOf(parentSpan.Context()))
	}

	return opentracing.
		StartSpan(t.String(), startSpanOptions...).
		SetTag("Type", t.Type())
}

func FromParentContext(t Type, parentContext opentracing.SpanContext) opentracing.Span {
	return opentracing.
		StartSpan(t.String(), opentracing.ChildOf(parentContext)).
		SetTag("Type", t.Type())
}

func FromParentContextWithTracer(
	t Type,
	parentContext opentracing.SpanContext,
	tracer opentracing.Tracer,
) opentracing.Span {
	return tracer.
		StartSpan(t.String(), opentracing.ChildOf(parentContext)).
		SetTag("Type", t.Type())
}

func ToContext(ctx context.Context, span opentracing.Span) context.Context {
	return opentracing.ContextWithSpan(ctx, span)
}

func WithSpan(ctx context.Context, t Type, exec func(span opentracing.Span)) {
	span := FromContext(ctx, t)

	exec(span)

	span.Finish()
}

// WithContext returns wrapped context.Context with injected in it span information and span itself.
func WithContext(ctx context.Context, t Type) (context.Context, opentracing.Span) {
	span := FromContext(ctx, t)

	return ToContext(ctx, span), span
}
