package jtracer_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/bonch.dev/go-lib/jtracer"
)

type testingStruct struct{}

func (*testingStruct) runType() jtracer.Type {
	return jtracer.WithType("Testing")
}

func TestNameOrTrace(t *testing.T) {
	t.Run("must return name of package, struct, and function of caller with additional type", func(t *testing.T) {
		assert.Equal(t, "Testing - jtracer_test/testingStruct/runType", (new(testingStruct)).runType().String())
	})
}
