package jtracer

import (
	"context"
	"net/http"

	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
)

// InjectHTTPHeaders injects current Span from context to http.Request
// May be useful for tracing between multiple microservices
func InjectHTTPHeaders(ctx context.Context, req *http.Request) error {
	span := extractSpanFromContext(ctx)

	if span == nil {
		return nil
	}

	carrier := opentracing.HTTPHeadersCarrier(req.Header)

	return errors.Wrap(
		span.Tracer().Inject(span.Context(), opentracing.HTTPHeaders, carrier),
		"failed to inject HTTP headers",
	)
}
