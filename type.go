package jtracer

import (
	"fmt"
	"runtime"
	"strings"
)

type Type struct {
	T string
	N string
}

func (t Type) String() string {
	if t.Type() == "" {
		return t.Name()
	}

	return fmt.Sprintf("%s - %s", t.T, t.N)
}

func (t Type) Type() string {
	return t.T
}

func (t Type) Name() string {
	return t.N
}

// WithType returns a Type, used for stacktrace (autogenerated name of span) with a subject
func WithType(subject string) Type {
	return Type{
		T: subject,
		N: traceName(1),
	}
}

func traceName(skip int) string {
	pc, _, _, ok := runtime.Caller(skip + 1)
	if !ok {
		return "unknown"
	}

	// It contains full path of pkg, caller and func
	details := runtime.FuncForPC(pc)

	functionFullNameS := strings.Split(details.Name(), "/")

	functionFullName := functionFullNameS[len(functionFullNameS)-1]

	functionFullName = strings.ReplaceAll(functionFullName, ".", "/")
	functionFullName = strings.ReplaceAll(functionFullName, "(", "")
	functionFullName = strings.ReplaceAll(functionFullName, ")", "")
	functionFullName = strings.ReplaceAll(functionFullName, "*", "")

	return functionFullName
}
